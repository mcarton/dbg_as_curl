use dbg_as_curl::dbg_as_curl;

fn compare(req: reqwest::RequestBuilder, result: &str) {
    let req = dbg_as_curl!(req);

    let req = req.build().unwrap();
    assert_eq!(format!("{}", dbg_as_curl::AsCurl::new(&req)), result);
}

#[test]
fn basic() {
    let client = reqwest::Client::new();

    compare(
        client.get("http://example.org"),
        "curl 'http://example.org/'",
    );
    compare(
        client.get("https://example.org"),
        "curl 'https://example.org/'",
    );
}

#[test]
fn escape_url() {
    let client = reqwest::Client::new();

    compare(
        client.get("https://example.org/search?q='"),
        "curl 'https://example.org/search?q=%27'",
    );
}

#[test]
fn bearer() {
    let client = reqwest::Client::new();

    compare(
        client.get("https://example.org").bearer_auth("foo"),
        "curl --header 'authorization: Bearer foo' 'https://example.org/'",
    );
}

#[test]
fn escape_headers() {
    let client = reqwest::Client::new();

    compare(
        client.get("https://example.org").bearer_auth("test's"),
        r"curl --header 'authorization: Bearer test'\''s' 'https://example.org/'",
    );
}

// The body cannot be included as there is not API to retrieve its content.
#[test]
fn body() {
    let client = reqwest::Client::new();

    compare(
        client.get("https://example.org").body("test's"),
        r"curl 'https://example.org/'",
    );
}