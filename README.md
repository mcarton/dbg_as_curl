# `dbg_as_curl` &emsp; [![Build Status]][CI] [![Latest Version]][crates.io] [![Documentation][docs-badge]][docs]

[Build Status]: https://gitlab.com/mcarton/dbg_as_curl/badges/master/pipeline.svg
[CI]: https://gitlab.com/mcarton/dbg_as_curl/commits/master
[Latest Version]: https://img.shields.io/crates/v/dbg_as_curl.svg
[crates.io]: https://crates.io/crates/dbg_as_curl
[docs-badge]: https://docs.rs/dbg_as_curl/badge.svg
[docs]: https://docs.rs/dbg_as_curl

A simple helper to get a [cURL] command line for your [`reqwest`][reqwest] calls:

```rust
let client = reqwest::Client::new();

dbg_as_curl!(
    client.get("https://example.org").bearer_auth("foo")
).send()?;
```

It works the same way as [the standard `dbg!`](https://doc.rust-lang.org/std/macro.dbg.html) macro, but will print a command line that you can copy&paste to execute a `reqwest` call yourself:

```
[tests/tests.rs:4] client.get("http://example.org").bearer_auth("foo") = curl --header 'authorization: Bearer foo' 'http://example.org/'
```

[cURL]: https://curl.haxx.se/
[reqwest]: https://crates.io/crates/reqwest