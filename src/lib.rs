/// A simple [`dbg!`](https://doc.rust-lang.org/std/macro.dbg.html)-like macro to help debugging `reqwest` calls.
///
/// ```rust
/// # use dbg_as_curl::dbg_as_curl;
/// let client = reqwest::Client::new();
///
/// dbg_as_curl!(
///     client.get("http://example.org").bearer_auth("foo")
/// ).send()?;
/// # Ok::<(), reqwest::Error>(())
/// ```
///
/// will display on the standard error output:
///
/// ```none
/// [tests/tests.rs:4] client.get("http://example.org").bearer_auth("foo") = curl --header 'authorization: Bearer foo' 'http://example.org/'
/// ```
#[macro_export]
macro_rules! dbg_as_curl {
    ($req:expr) => {
        // Use of `match` here is intentional because it affects the lifetimes
        // of temporaries - https://stackoverflow.com/a/48732525/1063961
        match $req {
            tmp => {
                match tmp.try_clone().map(|b| b.build()) {
                    Some(Ok(req)) => eprintln!(
                        "[{}:{}] {} = {}",
                        file!(),
                        line!(),
                        stringify!($req),
                        $crate::AsCurl::new(&req)
                    ),
                    Some(Err(err)) => eprintln!(
                        "[{}:{}] {} = *Error*: {}",
                        file!(),
                        line!(),
                        stringify!($req),
                        err
                    ),
                    None => eprintln!(
                        "[{}:{}] {} = *Error*: request not cloneable",
                        file!(),
                        line!(),
                        stringify!($req)
                    ),
                }

                tmp
            }
        }
    };
}

/// A wrapper around a request that displays as a cURL command.
pub struct AsCurl<'a> {
    req: &'a reqwest::Request,
    compress: bool,
    verbose: bool,
}

impl<'a> AsCurl<'a> {
    /// Construct an instance of `AsCurl` with the given request.
    pub fn new(req: &'a reqwest::Request) -> AsCurl<'a> {
        Self {
            req,
            compress: false,
            verbose: false,
        }
    }

    /// Adds '--compress' to the command line.
    pub fn compress(self) -> Self {
        Self {
            compress: true,
            ..self
        }
    }

    /// Adds '--verbose' to the command line.
    pub fn verbose(self) -> Self {
        Self {
            verbose: true,
            ..self
        }
    }
}

impl<'a> std::fmt::Debug for AsCurl<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        <Self as std::fmt::Display>::fmt(self, f)
    }
}

impl<'a> std::fmt::Display for AsCurl<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let AsCurl {
            req,
            compress,
            verbose,
        } = *self;

        write!(f, "curl ")?;

        if compress {
            write!(f, "--compress ")?;
        }
        if verbose {
            write!(f, "--verbose ")?;
        }

        let method = req.method();
        if method != "GET" {
            write!(f, "-X {} ", method)?;
        }

        for (name, value) in req.headers() {
            let value = value
                .to_str()
                .expect("Headers must contain only visible ASCII characters")
                .replace("'", r"'\''");

            write!(f, "--header '{}: {}' ", name, value)?;
        }

        if let Some(_) = req.body() {
            log::warn!("dbg_as_curl cannot show request's body");
        }

        write!(f, "'{}'", req.url().to_string().replace("'", "%27"))?;

        Ok(())
    }
}